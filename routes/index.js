var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var os = require("os");
  res.render('index', { 
    title: 'My express website',
    hostname: os.hostname(),
    ipAddress: req.connection.localAddress
   });
});

module.exports = router;
